package com.ahc.app;

import com.ahc.app.helper.GlobalVar;

import android.app.Activity;
import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

public class TabHostActivity extends TabActivity {
	GlobalVar var;
	String varAHC;
	Drawable iconAHC;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tabhost);
		var = ((GlobalVar) getApplicationContext());
		Log.i("var glob", String.valueOf(var.getSelected()));

		switch (var.getSelected()) {
		case 0:
			varAHC = "Rumah Sakit";
			iconAHC = this.getResources().getDrawable(R.drawable.rs32);
			break;

		case 1:
			varAHC = "Klinik";
			iconAHC = this.getResources().getDrawable(R.drawable.klinik32);
			break;
		case 2:
			varAHC = "Apotek";
			iconAHC = this.getResources().getDrawable(R.drawable.apotek32);
			break;
		case 3:
			varAHC = "Tips Sehat";
			iconAHC = this.getResources().getDrawable(R.drawable.tips32);
			break;
		}

		Resources ressources = getResources();
		TabHost tabHost = getTabHost();

		// Android tab
		Intent intentAndroid = new Intent().setClass(this,
				ListAHC_Activity.class);
		TabSpec tabSpecAndroid = tabHost.newTabSpec("AHC")
				.setIndicator(varAHC, iconAHC).setContent(intentAndroid);

		// Apple tab
		Intent intentApple = new Intent().setClass(this, MapAHC_Activity.class);
		TabSpec tabSpecApple = tabHost
				.newTabSpec("Sekitar Saya")
				.setIndicator("Sekitar Saya",
						ressources.getDrawable(R.drawable.marker32))
				.setContent(intentApple);

		// add all tabs
		tabHost.addTab(tabSpecAndroid);
		tabHost.addTab(tabSpecApple);

		// set Windows tab as default (zero based)
		tabHost.setCurrentTab(0);
	}

}
