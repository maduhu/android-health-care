package com.ahc.app;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class TipsActivity extends Activity {

	WebView w;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tips);

		w = (WebView) findViewById(R.id.webView1);
		w.loadUrl("file:///android_asset/tips.html");
	}

}
