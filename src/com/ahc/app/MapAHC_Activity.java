package com.ahc.app;

import java.io.IOException;
import java.util.ArrayList;

import com.ahc.app.helper.AHCBaseAdapter;
import com.ahc.app.helper.DataBaseHelper;
import com.ahc.app.helper.EntitasTempat;
import com.ahc.app.helper.GlobalVar;
import com.ahc.app.helper.OnRouteCalcCompleted;
import com.ahc.app.helper.RouteHandler;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.LocationSource.OnLocationChangedListener;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.SQLException;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MapAHC_Activity extends FragmentActivity implements
		LocationListener, LocationSource, OnMarkerClickListener,
		OnRouteCalcCompleted {
	GoogleMap myMap;
	LatLng jkt = new LatLng(-6.201739, 106.851727);
	LocationManager lm;
	OnLocationChangedListener lc;
	Marker mUser;
	MarkerOptions marker_ahc;
	Button bdir;
	private DataBaseHelper db;
	GlobalVar var;
	boolean srcdetected = false;
	boolean destdetected = false;
	double latuser, lonuser;
	LatLng loctkp;

	private RouteHandler routeHandler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		var = ((GlobalVar) getApplicationContext());
		db = new DataBaseHelper(this);
		try {
			db.createDataBase();
		} catch (IOException ioe) {
			throw new Error("Unable to create database");
		}

		try {
			db.openDataBase();
		} catch (SQLException sqle) {
			throw sqle;
		}

		bdir = (Button) findViewById(R.id.bdirection);
		bdir.setVisibility(View.GONE);
		try {
			MapsInitializer.initialize(MapAHC_Activity.this);
		} catch (GooglePlayServicesNotAvailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setUpMapIfNeeded();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}


	private void ambildata() {
		// TODO Auto-generated method stub
		ArrayList<ArrayList<Object>> data = db.AmbilKategori(var.getSelected());
		Log.i("data", String.valueOf(data.size()));
		if (data.size() > 0) {
			for (int i = 0; i < data.size(); i++) {
				ArrayList<Object> b = data.get(i);

				// entitastempat = new EntitasTempat();
				// entitastempat
				// .setIDtempat(Integer.parseInt(b.get(0).toString()));
				// entitastempat.setNamatempat(b.get(1).toString());
				// entitastempat.setAlamattempat(b.get(2).toString());
				// entitastempat.setNope(b.get(3).toString());
				// tempat.add(entitastempat);

				double lat = Double.parseDouble(b.get(4).toString());
				double lon = Double.parseDouble(b.get(5).toString());
				setTempat(lat, lon, b.get(1).toString(), b.get(2).toString(),
						var.getSelected());
			}
		} else {
			Toast.makeText(this, "no data", Toast.LENGTH_SHORT).show();

		}
	}

	public boolean isGoogleMapsInstalled() {
		try {
			ApplicationInfo info = getPackageManager().getApplicationInfo(
					"com.google.android.apps.maps", 0);
			Log.i("TAG", info.packageName);
			return true;
		} catch (PackageManager.NameNotFoundException e) {
			return false;
		}
	}

	private void setUpMapIfNeeded() {
		if (myMap == null) {
			// SupportMapFragment fm= (SupportMapFragment)
			// getSupportFragmentManager().findFragmentById(R.id.map);

			myMap = ((SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.map)).getMap();

			if (isGoogleMapsInstalled()) {
				if (myMap != null) {
					// manipulate the map

					myMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
					myMap.animateCamera(CameraUpdateFactory.newLatLngZoom(jkt,
							13));

					DetectUserLocation();
					ambildata();
				}
			} else {
				Builder builder = new AlertDialog.Builder(this);
				builder.setMessage("Please install Google Maps");
				builder.setCancelable(false);
				builder.setPositiveButton("Install",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								Intent intent = new Intent(
										Intent.ACTION_VIEW,
										Uri.parse("market://details?id=com.google.android.apps.maps"));
								startActivity(intent);

								// Finish the activity so they can't circumvent
								// the check
								finish();
							}
						});
				AlertDialog dialog = builder.create();
				dialog.show();
			}
		}
	}

	private void DetectUserLocation() {
		// TODO Auto-generated method stub
		lm = (LocationManager) getSystemService(LOCATION_SERVICE);
		if (lm != null) {
			boolean gps_is_enabled = lm
					.isProviderEnabled(LocationManager.GPS_PROVIDER);
			boolean network_is_enabled = lm
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
			// Creating a criteria object to retrieve provider
			Criteria criteria = new Criteria();

			// Getting the name of the best provider
			String provider = lm.getBestProvider(criteria, true);

			// Getting Current Location
			Location location = lm.getLastKnownLocation(provider);
			// Location location = lm
			// .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
			if (location != null) {
				// PLACE THE INITIAL MARKER
				setposisiku(location.getLatitude(), location.getLongitude());
			}
			if (gps_is_enabled) {
				lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 100,
						1000, this);
			} else if (network_is_enabled) {
				lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
						100, 1000, this);
			}
			myMap.setLocationSource(this);
			myMap.setMyLocationEnabled(true);
		}
	}

	private void setposisiku(double latitude, double longitude) {
		// TODO Auto-generated method stub

		srcdetected = true;
		if (mUser != null) {
			mUser.remove();
		}
		latuser = latitude;
		lonuser = longitude;
		LatLng user = new LatLng(latitude, longitude);

		MarkerOptions marker_user = new MarkerOptions();
		marker_user.position(user);
		marker_user.title("posisisku");
		marker_user.snippet("aku disini");
		marker_user.icon(BitmapDescriptorFactory
				.fromResource(R.drawable.marker32));

		CircleOptions circleOptions = new CircleOptions().center(user) // set
				// center
				.radius(1000) // set radius in meters
				.fillColor(0x40ff0000) // semi-transparent
				.strokeColor(Color.BLUE).strokeWidth(1);

		// myCircle = myMap.addCircle(circleOptions);

		mUser = myMap.addMarker(marker_user);
	}

	protected void setTempat(double latitude, double longitude, String nama,
			String almt, int kat) {
		// TODO Auto-generated method stub

		LatLng tempat = new LatLng(latitude, longitude);

		marker_ahc = new MarkerOptions();
		marker_ahc.position(tempat);
		marker_ahc.title(nama);
		marker_ahc.snippet(almt);
		switch (kat) {
		case 0:
			marker_ahc.icon(BitmapDescriptorFactory
					.fromResource(R.drawable.rs32));

			break;

		case 1:
			marker_ahc.icon(BitmapDescriptorFactory
					.fromResource(R.drawable.klinik32));
			break;
		case 2:
			marker_ahc.icon(BitmapDescriptorFactory
					.fromResource(R.drawable.apotek32));
			break;
		}
		marker_ahc.visible(true);
		myMap.setOnMarkerClickListener(this);
		myMap.addMarker(marker_ahc);
		myMap.animateCamera(CameraUpdateFactory.newLatLngZoom(tempat, 16));
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		if (lc != null) {
			lc.onLocationChanged(location);

			LatLng lokasibaru = new LatLng(location.getLatitude(),
					location.getLongitude());
			setposisiku(lokasibaru.latitude, lokasibaru.longitude);
			// myMap.animateCamera(CameraUpdateFactory.newLatLngZoom(lokasibaru,
			// 15));

		}
	}

	@Override
	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void activate(OnLocationChangedListener listener) {
		// TODO Auto-generated method stub
		lc = listener;
	}

	@Override
	public void deactivate() {
		// TODO Auto-generated method stub
		lc = null;

	}

	@Override
	public boolean onMarkerClick(Marker m) {
		// TODO Auto-generated method stub
		Log.d("marker", String.valueOf(m.getPosition()));
		Log.d("marker", m.getTitle());
		ShowDetil(m);
		return true;
	}

	protected void ShowDetil(Marker m) {
		// TODO Auto-generated method stub
		destdetected = true;
		loctkp = m.getPosition();

		final Dialog d = new Dialog(this);
		d.setTitle("Info Detail");

		d.setContentView(R.layout.custom_dialog);
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(d.getWindow().getAttributes());
		lp.width = LayoutParams.MATCH_PARENT;
		lp.height = LayoutParams.WRAP_CONTENT;

		d.getWindow().setAttributes(lp);
		Button bok = (Button) d.findViewById(R.id.bOk);
		bok.setText("direction");
		ImageView im = (ImageView) d.findViewById(R.id.icon);
		im.setVisibility(View.GONE);
		TextView tnama = (TextView) d.findViewById(R.id.nama_txt);
		TextView taddr = (TextView) d.findViewById(R.id.alamat_txt);
		TextView tphone = (TextView) d.findViewById(R.id.phone_txt);
		TextView t3 = (TextView) d.findViewById(R.id.TextView03);
		tphone.setVisibility(View.GONE);
		t3.setVisibility(View.GONE);
		im.setImageResource(R.drawable.apotek32);
		tnama.setText(" : " + m.getTitle());
		taddr.setText(" : " + m.getSnippet());

		// Intent iCall = new Intent(Intent.ACTION_CALL);
		// iCall.setData(Uri.parse("tel:" + string3));
		// startActivity(iCall);
		bok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				d.dismiss();
				if (srcdetected && destdetected) {
					cetakrute(latuser, lonuser, loctkp.latitude,
							loctkp.longitude);
				} else {
					Toast.makeText(
							MapAHC_Activity.this,
							"Your location not ready. \n Direction not available",
							Toast.LENGTH_SHORT).show();
				}
			}
		});
		d.show();

	}

	protected void cetakrute(double latuser2, double lonuser2, double latitude,
			double longitude) {
		// TODO Auto-generated method stub
		routeHandler = new RouteHandler(MapAHC_Activity.this, this);
		routeHandler.calculateRoute(latuser2, lonuser2, latitude, longitude,
				RouteHandler.FINE_ROUTE);
	}

	@Override
	public void onRouteCalcBegin() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();

		myMap.setLocationSource(null);
		lm.removeUpdates(this);
	}

	@Override
	public void onRouteCompleted(ArrayList route) {
		// TODO Auto-generated method stub

		myMap.addPolyline((new PolylineOptions().color(Color.BLUE).width(5))
				.addAll(route));
	}

}
