package com.ahc.app.helper;

import java.util.ArrayList;

public interface OnRouteCalcCompleted {
	void onRouteCalcBegin();

	@SuppressWarnings("rawtypes")
	void onRouteCompleted(ArrayList route);
}