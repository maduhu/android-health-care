package com.ahc.app.helper;

public class EntitasTempat {
	int idtempat;
	String namatempat = "";
	String alamattempat = "";
	String nope = "0";
	String latitude = "0";
	String longitude = "0";

	public void setLon(String lo) {
		this.longitude = lo;
	}

	public String getLon() {
		return longitude;
	}

	public void setLat(String l) {
		this.latitude = l;
	}

	public String getLat() {
		return latitude;
	}

	public void setNope(String n) {
		this.nope = n;
	}

	public String getNope() {
		return nope;
	}

	public void setIDtempat(int id) {
		this.idtempat = id;
	}

	public int getIDtempat() {
		return idtempat;
	}

	public void setNamatempat(String n) {
		this.namatempat = n;
	}

	public String getNamatempat() {
		return namatempat;
	}

	public void setAlamattempat(String a) {
		this.alamattempat = a;
	}

	public String getAlamattempat() {
		return alamattempat;
	}

}
