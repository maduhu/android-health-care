package com.ahc.app.helper;

import android.app.Application;

public class GlobalVar extends Application {
	private Boolean _notification = false;
	private int selected = 0;

	public Boolean get_notification() {
		return _notification;
	}

	public void set_notification(Boolean _notification) {
		this._notification = _notification;
	}

	public int getSelected() {
		return selected;
	}

	public void setSelected(int s) {
		this.selected = s;
	}
}