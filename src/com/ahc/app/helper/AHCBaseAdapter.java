package com.ahc.app.helper;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import com.ahc.app.R;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

public class AHCBaseAdapter extends BaseAdapter {
	private static ArrayList<EntitasTempat> searchArrayList;
	Context c;
	private LayoutInflater mInflater;
	Bitmap bm;

	public AHCBaseAdapter(Context context, ArrayList<EntitasTempat> results) {
		searchArrayList = results;
		this.mInflater = LayoutInflater.from(context);
		this.c = context;

	}

	@Override
	public int getCount() {
		return searchArrayList.size();
	}

	@Override
	public Object getItem(int position) {
		return searchArrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.customlist_item, null);
			holder = new ViewHolder();

			holder.nama = (TextView) convertView.findViewById(R.id.nama_ahc);
			holder.alamat = (TextView) convertView
					.findViewById(R.id.alamat_ahc);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		if (searchArrayList.get(position).getNamatempat().length() > 20) {

			holder.nama.setText(searchArrayList.get(position).getNamatempat()
					.substring(0, 20)
					+ "...");
		} else {
			holder.nama.setText(searchArrayList.get(position).getNamatempat());

		}
		if (searchArrayList.get(position).getNamatempat().length() > 30) {

			holder.alamat.setText(searchArrayList.get(position)
					.getAlamattempat().substring(0, 30)
					+ "...");
		} else {
			holder.alamat.setText(searchArrayList.get(position)
					.getAlamattempat());

		}
		return convertView;
	}

	static class ViewHolder {
		TextView nama, alamat;

	}

}